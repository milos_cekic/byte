(defun findIfResult (lista initialposition)
;ova funkcija proverava da li inicijalna pozicija postoji u listi, koristimo je kod proveravanja da li potez postoji u listi
;validnih poteza
(cond ((null initialposition) '() )
    ((null lista) lista)

( (and  (equalp  (caar lista) (car initialposition)) (equalp  (cadar lista) (cadr initialposition) ) )
    (cdar lista)
 )

(t (findIfResult (cdr lista) initialposition ))
)

)

(defun findPositionsForNextPlayingForTheGivenRow (rowIndex row nextPlaying n positionlist)

(cond ((null nextPlaying) '() )
    ((null row) positionlist)

((member nextPlaying (cadar row))
    (append positionlist (list  (list rowIndex (caar row) (length (removeLines (member nextPlaying (cadar row)) ))) )
 (findPositionsForNextPlayingForTheGivenRow rowIndex (cdr row) nextPlaying n positionlist)) 
    )

(t 
(findPositionsForNextPlayingForTheGivenRow rowIndex (cdr row) nextPlaying n positionlist))
)
)

(defun findPositionsForNextPlaying (lista nextPlaying n positionlist)
(cond ((null nextPlaying) positionlist )
    ((null lista) positionlist )

(t 

(append positionlist
         (findPositionsForNextPlayingForTheGivenRow (caar lista) (cdar lista) nextPlaying n positionlist)
        (findPositionsForNextPlaying (cdr lista) nextPlaying n positionlist)

)
    )
)
)

(defun FindAllPossiblePlaysTraverse (lista listfortraversing nextPlaying n resultList)
(cond
    ((null listfortraversing) resultList )
    ((null nextPlaying) resultList )
    
    (t 

 ;(and 
;    (princ "usao sam")
;    (princ (list  (caar listfortraversing) (cadar listfortraversing)) )
 ;   (princ (caddar listfortraversing))
;    (princ (findPositionsForNextPlaying lista nextPlaying n '()))
    ;(findAllPossiblePlaysForPosition lista '(4 4) '(4 4) 8 1 '() 0)
;(princ (remove-duplicates   (findAllPossiblePlaysForPosition lista  (list  (caar listfortraversing) (cadar listfortraversing)) (list  (caar listfortraversing) (cadar listfortraversing)) n (caddar listfortraversing) '() 0)   :test 'equal) )
;(princ #\newline)
;(FindAllPossiblePlaysTraverse lista (cdr listfortraversing) nextPlaying n resultList)
;   )

(append resultList (car listfortraversing)
 (remove-duplicates   (findAllPossiblePlaysForPosition lista  (list  (caar listfortraversing) (cadar listfortraversing)) (list  (caar listfortraversing) (cadar listfortraversing)) n (caddar listfortraversing) '() 0)   :test 'equal) 

(FindAllPossiblePlaysTraverse lista (cdr listfortraversing) nextPlaying n resultList)
   ;(princ  (remove-duplicates (findAllPossiblePlaysForPosition lista  (cdar listfortraversing) (cdar listfortraversing) n (caddar listfortraversing) '() 0) :test 'equal)
   )
 )
)
)

(defun FindAllPossiblePlays (lista nextPlaying n)

    (FindAllPossiblePlaysTraverse lista (findPositionsForNextPlaying lista nextPlaying n '()) nextPlaying n '())
)

(defun findAllPossiblePlaysForPosition (lista initialposition position n height resultList counter)
    (cond
        ((not (null resultList)) (remove-duplicates resultList))

;        ((null resultList)
 ;           (append resultList (modifyField (findClosestStacks lista initialposition position resultList n ))
  ;          (findAllPossiblePlaysForPosition lista initialposition (list  (- (car position) 1) (- (cadr position) 1) ) n height resultList )
   ;         (findAllPossiblePlaysForPosition lista initialposition (list  (- (car position) 1) (+ (cadr position) 1) ) n height resultList )
    ;        (findAllPossiblePlaysForPosition lista initialposition (list  (+ (car position) 1) (- (cadr position) 1) ) n height resultList )
     ;       (findAllPossiblePlaysForPosition lista initialposition (list  (+ (car position) 1) (+ (cadr position) 1) ) n height resultList )            
      ;       )
       ; )
;        ((findIfResult resultList initialposition)
            ;izbacimo iz resulist i ponovo pozovemo ali za njegove  
        
 ;       )

       ((and (null resultList) (equal initialposition position) )
      
               (setq nekaLista (modifyField (findClosestStacks lista position initialposition resultList n counter )) )
       (if (not (null nekaLista))
         (findAllPossiblePlaysForPosition lista initialposition position n height nekaLista counter)
        
           (remove-duplicates (append resultList (findAllPossiblePlaysForPosition lista position (list  (- (car position) 1) (- (cadr position) 1) ) n height (modifyField (findClosestStacks lista  (list  (- (car position) 1) (- (cadr position) 1) ) position resultList n (+ 1 counter))) counter )
            (findAllPossiblePlaysForPosition lista position (list  (- (car position) 1) (+ (cadr position) 1) ) n height  (modifyField (findClosestStacks lista (list  (- (car position) 1) (+ (cadr position) 1) ) position resultList n (+ 1 counter)) ) counter)
            (findAllPossiblePlaysForPosition lista position (list  (+ (car position) 1) (- (cadr position) 1) ) n height (modifyField (findClosestStacks lista (list  (+ (car position) 1) (- (cadr position) 1) ) position resultList n (+ 1 counter))) counter) 
            (findAllPossiblePlaysForPosition lista position (list  (+ (car position) 1) (+ (cadr position) 1) ) n height (modifyField (findClosestStacks lista (list  (+ (car position) 1) (+ (cadr position) 1) ) position resultList n (+ 1 counter) )) counter)
           )
           )
           
       
       )
        
       
       )
         
         ((null resultList)
            
            (remove-duplicates (modifyField  (append resultList (findAllPossiblePlaysForPosition lista position (list  (- (car position) 1) (- (cadr position) 1) ) n height (findClosestStacks lista  (list  (- (car position) 1) (- (cadr position) 1) ) position resultList n (+ 1 counter)) counter)
            (findAllPossiblePlaysForPosition lista position (list  (- (car position) 1) (+ (cadr position) 1) ) n height  (findClosestStacks lista (list  (- (car position) 1) (+ (cadr position) 1) ) position resultList n (+ 1 counter)) counter)
            (findAllPossiblePlaysForPosition lista position (list  (+ (car position) 1) (- (cadr position) 1) ) n height  (findClosestStacks lista (list  (+ (car position) 1) (- (cadr position) 1) ) position resultList n (+ 1 counter)) counter) 
            (findAllPossiblePlaysForPosition lista position (list  (+ (car position) 1) (+ (cadr position) 1) ) n height  (findClosestStacks lista (list  (+ (car position) 1) (+ (cadr position) 1) ) position resultList n (+ 1 counter)) counter) )
            )
            )
        ;  (findAllPossiblePlaysForPosition lista initialposition position n height   (append resultList (modifyField (findClosestStacks lista (list  (- (car position) 1) (+ (cadr position) 1) ) position resultList n ))
         ;        (modifyField (findClosestStacks lista (list  (+ (car position) 1) (- (cadr position) 1) ) position resultList n ))
          ;       (modifyField (findClosestStacks lista (list  (- (car position) 1) (- (cadr position) 1) ) position resultList n ))
           ;      (modifyField (findClosestStacks lista (list  (+ (car position) 1) (+ (cadr position) 1) ) position resultList n ))
            ; )
          ;)  
        )   
         ; )
         
       ;(t resultList)
        
        ;(t (
         ;   (and 
          ;     (setq resultList (findIfResult (modifyField (findClosestStacks lista initialposition resultList n )) initialposition) )
            
        ;   (append (findAllPossiblePlaysForPosition lista initialposition (list  (- (car initialposition) 1) (- (cadr initialposition) 1) ) n height resultList )
         ;   (findAllPossiblePlaysForPosition lista initialposition (list  (- (car initialposition) 1) (+ (cadr initialposition) 1) ) n height resultList )
          ;  (findAllPossiblePlaysForPosition lista initialposition (list  (+ (car initialposition) 1) (- (cadr initialposition) 1) ) n height resultList )
           ; (findAllPossiblePlaysForPosition lista initialposition (list  (+ (car initialposition) 1) (+ (cadr initialposition) 1) ) n height resultList )
        

            ;)
           
         ; )
        ;)
       ;)

       
    )
)

(defun modifyField (listForModifing)

        (if (null listForModifing) '()

        (if (null (car listForModifing)) 
            (modifyField (cdr listForModifing))
            
        (if (zerop (caddar listForModifing)) 
            (modifyField (cdr listForModifing)) 
            (append (list  (car listForModifing) ) (modifyField (cdr listForModifing) ))
        )
        
        )
        )
)

(defun findClosestStacks (lista positionForSearching startinPosition possiblePlays n counter)
    (cond
        ((null lista ) possiblePlays)
        ((null positionForSearching ) possiblePlays)        

        ( (equal startinPosition (list (- (car positionForSearching) 1) (- (cadr positionForSearching) 1)))
                (append possiblePlays 
                    (list  (test lista (list (- (car positionForSearching) 1) (+ (cadr positionForSearching) 1)) possiblePlays n counter))
                    (list  (test lista (list (+ (car positionForSearching) 1) (- (cadr positionForSearching) 1)) possiblePlays n counter))
                    (list  (test lista (list (+ (car positionForSearching) 1) (+ (cadr positionForSearching) 1)) possiblePlays n counter))            
                )

        )

       ( (equal startinPosition (list (- (car positionForSearching) 1) (+ (cadr positionForSearching) 1)))
            (append possiblePlays 
                (list  (test lista (list (- (car positionForSearching) 1) (- (cadr positionForSearching) 1)) possiblePlays n counter))
                (list  (test lista (list (+ (car positionForSearching) 1) (- (cadr positionForSearching) 1)) possiblePlays n counter))
                (list  (test lista (list (+ (car positionForSearching) 1) (+ (cadr positionForSearching) 1)) possiblePlays n counter))            
            )
       )
        ( (equal startinPosition (list (+ (car positionForSearching) 1) (- (cadr positionForSearching) 1)))
            (append possiblePlays 
                (list  (test lista (list (- (car positionForSearching) 1) (- (cadr positionForSearching) 1)) possiblePlays n counter))
                (list  (test lista (list (- (car positionForSearching) 1) (+ (cadr positionForSearching) 1)) possiblePlays n counter))
                (list  (test lista (list (+ (car positionForSearching) 1) (+ (cadr positionForSearching) 1)) possiblePlays n counter))            
            )

        )

       ( (equal startinPosition (list (+ (car positionForSearching) 1) (+ (cadr positionForSearching) 1)))
            (append possiblePlays 
                (list  (test lista (list (- (car positionForSearching) 1) (- (cadr positionForSearching) 1)) possiblePlays n counter))
                (list  (test lista (list (- (car positionForSearching) 1) (+ (cadr positionForSearching) 1)) possiblePlays n counter))
                (list  (test lista (list (+ (car positionForSearching) 1) (- (cadr positionForSearching) 1)) possiblePlays n counter))            
            )
       )
        (t 
            (append possiblePlays (list  (test lista (list (- (car positionForSearching) 1) (- (cadr positionForSearching) 1)) possiblePlays n counter))
            (list  (test lista (list (- (car positionForSearching) 1) (+ (cadr positionForSearching) 1)) possiblePlays n counter))
            (list  (test lista (list (+ (car positionForSearching) 1) (- (cadr positionForSearching) 1)) possiblePlays n counter))
            (list  (test lista (list (+ (car positionForSearching) 1) (+ (cadr positionForSearching) 1)) possiblePlays n counter))            
            ) 
        )
    )
)



(defun test (lista positionForSearching possiblePlays n counter)

    (cond
        ((null lista ) possiblePlays)
        ((null positionForSearching ) possiblePlays)
    
    (t
            
            (if 
            (and (>= (car positionForSearching) 1) (<= (car positionForSearching) n)
            (>= (cadr positionForSearching) 1) (<= (cadr positionForSearching) n) ) 
              (append possiblePlays  (list (car positionForSearching) (cadr positionForSearching) 
                 (length   (removeLines  (findStack lista (list (car positionForSearching) (cadr positionForSearching) ) ) ) ) (+ counter 1 ) ))
    
            (append possiblePlays '() )
            )
    )
    )
)





(defun getPlayFromConsole ()
 (cond  
    ((not (isNumberOfStackGreaterThanHalf))
(princ "Please enter the move in the specified way: ( (E 3) (F 4) 0) : ")
(setq move (read))
(cond

    ((and (listp (car move)) (listp (cadr move)) (atom (caddr move)))  
    (play lista (car move) (cadr move) (caddr move)))
    (t 
       (princ "Invalid move.")
       (getPlayFromConsole)
       )

)
    )
    (t
    (if (< numberOfStacksX numberOfStacksO)
        (princ "O is a winner.")

        (princ "X is a winner.")
    )
    )
 )
)

(defun findStackForTheGivenRow (row position)

(cond ((null position) (princ "Invalid Play.")  )
    ((null row) (princ "Invalid Play."))

( (equalp  (caar row) (cadr position)) 
    (cadar row)
 )

(t (findStackForTheGivenRow (cdr row) position ))
)

)



(defun findStack (lista position)
(cond ((null position) (princ "Invalid Play.")  )
    ((null lista) (princ "Invalid Play."))

( (equalp  (caar lista) (car position))
 
    (let ( (resultStack (findStackForTheGivenRow (cdar lista ) position))  )    resultStack    )
      
 )

(t (findStack (cdr lista) position ))))


(defun charToNumber (ch)
	(let
		((characterCode (char-code (coerce ch 'character))))
		(cond
			((> characterCode 90) '())
			((< characterCode 65) '())
			('t (- characterCode 64))  ;promenjeno je u 64 zbog nase liste koja pocinje od 1
		)
	)
)

(defun play(lista  current next height)
  
 (cond 

    ( (null (findIfResult         
        (MakeListOfAllPossiblePlaysForPlayer lista (remove-duplicates 
        (ModifyListOfLegalPlaysTraverse (ModifyCounter (ModifyListOfPossiblePlays '() (FindAllPossiblePlays lista nextPlaying n) '()) '() '()) '())
        :test 'equal)
         n nextPlaying '())  
        (list (list  (charToNumber  (car current))  (cadr current)  height) 
                        (list  (charToNumber  (car next))  (cadr next) 
                            (length  (removeLines (findStack lista (list  (charToNumber  (car next))  (cadr next)  ))) ))
                        )
    ))
    ;tacno i mogu 2 uslova zasto je nevalidni potez
        (if (null
        (MakeListOfAllPossiblePlaysForPlayer lista (remove-duplicates 
        (ModifyListOfLegalPlaysTraverse (ModifyCounter (ModifyListOfPossiblePlays '() (FindAllPossiblePlays lista nextPlaying n) '()) '() '()) '())
        :test 'equal)
         n nextPlaying '()) 
            )
                    ;tacno, treba propustiti potez jer nema validnog poteza
            (and 
                (if (equalp nextPlaying 'X)
                    (setq nextPlaying 'O)

                    (setq nextPlaying 'X)
                )   
                (princ "Invalid play. There is not valid moves for you to play.")   
               (getPlayFromConsole)
            )
            (and         
                ;nevalidno odigran potez
                (princ "Invalid play.") 
                (getPlayFromConsole)
            )
        )
      
    )
    (t 
    ;validan potez
                    (if (equalp 8 (length (pushOnStack (removeLines (findStack lista (list  (charToNumber  (car next))  (cadr next)  )))
                      (makeTmpStack (removeLines  (findStack lista (list  (charToNumber  (car current))  (cadr current)  ))  ) '() 0 
                      (- (length (removeLines  (findStack lista (list  (charToNumber  (car current))  (cadr current)  ))  )) height) ) )) )
                    
                    (and 
                            (if (equalp 'X (car (pushOnStack (removeLines (findStack lista (list  (charToNumber  (car next))  (cadr next)  )))  (makeTmpStack (removeLines  (findStack lista (list  (charToNumber  (car current))  (cadr current)  ))  ) '() 0 (- (length (removeLines  (findStack lista (list  (charToNumber  (car current))  (cadr current)  ))  )) height) ) )) )
                                (setq numberOfStacksX (+ numberOfStacksX 1))
                                
                                (setq numberOfStacksO (+ numberOfStacksO 1))
                            )

                            (findStackForChange lista (list  (charToNumber  (car next))  (cadr next)  ) (addLines '() ))
                    )

                            (findStackForChange lista (list  (charToNumber  (car next))  (cadr next)  ) (addLines (pushOnStack (removeLines (findStack lista (list  (charToNumber  (car next))  (cadr next)  )))  (makeTmpStack (removeLines  (findStack lista (list  (charToNumber  (car current))  (cadr current)  ))  ) '() 0 (- (length (removeLines  (findStack lista (list  (charToNumber  (car current))  (cadr current)  ))  )) height) ) ) ))
                     )

                    (findStackForChange lista (list  (charToNumber  (car current))  (cadr current)  ) (addLines (makeCurrentStack (removeLines  (findStack lista (list  (charToNumber  (car current))  (cadr current)  ))  ) (- (length (removeLines  (findStack lista (list  (charToNumber  (car current))  (cadr current)  ))  )) height) )))

                    (printWholePicture 8 lista)
                    (if (equalp nextPlaying 'X)
                        (setq nextPlaying 'O)

                        (setq nextPlaying 'X)
                    )

                    (princ #\newline)
                    (princ "Next playing: ")
                    (princ nextPlaying)
                    (princ #\newline)
                    (getPlayFromConsole)

                )
            
          )      
      
)


(defun makeCurrentStack (stack height)
    (cond 
     ((null stack) stack) 
    
    ((zerop height) stack )
        (t 
        (makeCurrentStack (cdr stack)  (- height 1))
        
        ) 
    )
)

(defun findStackForChangeForTheGivenRow (row position changingstack)

(cond ((null position) (princ "Invalid Play.")  )
    ((null row) (princ "Invalid Play."))

( (equalp  (caar row) (cadr position))
 
 (replace (cadar row) changingstack :start1 0 :end1 9  :start2 0 :end2 9)
   
 )

(t (findStackForChangeForTheGivenRow (cdr row) position changingstack ))
)

)

(defun findStackForChange (lista position changingstack)
(cond ((null position) (princ "Invalid Play.")  )
    ((null lista) (princ "Invalid Play."))

( (equalp  (caar lista) (car position))
 
    (let ( (resultStack (findStackForChangeForTheGivenRow (cdar lista ) position  changingstack ))  )    resultStack    )
      
 )

(t (findStackForChange (cdr lista) position changingstack ))))


(defun makeTmpStack (stack tmpstack counter height)
    (cond 
     ((null stack) tmpstack) 
    
    ((equalp height counter) tmpstack )
        (t 
            (makeTmpStack (cdr stack) (cons (car stack) tmpstack ) (+ counter 1) height ) 
        ) 
    )
)



(defun pushOnStack (stack tmpstack)
    (cond 
     ((null tmpstack) stack)

        (t 
            (pushOnStack (cons (car tmpstack) stack ) (cdr tmpstack)) 
        ) 
    )
)


;popunjava crtice u stacku
(defun addLines (stack)

    (cond 
    ((> 9 (length stack))
        (addLines (cons '- stack) )
    )
    (t stack
    )
  )
)

;skida crtice u stacku
(defun removeLines (stack)

    (cond 
    ((equalp '- (car stack))
        (removeLines (cdr stack) )
    )
    (t stack
    )
  )
)

; pravi veliku konacnu listu u kojoj je sve smesteno
(defun createTable (n br1 br2 lista)
(cond
   ((> br1 n)
      lista  
   )
   ((equalp 1 br1)
      (append lista (list (createRow n br1 br2 lista))  (createTable n (+ 1 br1) br2 lista ))

   )
   (t 
    (append lista (list (createRow n br1 br2 lista)) (createTable n (+ 1 br1) br2 lista))
    
)
)
)

;ova lista je zapravo jedan red
(defun createRow (n br1 br2 lista)
     (cond       
     ((>= n  br2)
         (cond
            
          ((equalp br2 1)

          (cond 
            ( (equalp br1 1)
              (append lista (list br1 ) (list (list (floor br2) (list '- '- '- '- '- '- '- '- '-)))  (createRow n br1 (+ br2 1) lista )   ) )
        
            (
                (and (not (zerop (mod br1 2))))
                 (append (list br1 ) (list (list (floor br2) (list '- '- '- '- '- '- '- '- 'O) ) )
                    (createRow n br1 (+ br2 1) lista))
            )
            (t 
                  (append (list br1 ) (list (list (floor br2) '())  )
                    (createRow n br1 (+ br2 1) lista))
            )
        
         )
          
          )
        ((equalp  br1 1)

          (if (not (zerop (mod br2 2))) 
                (append  (list (list  (floor br2) (list '- '- '- '- '- '- '- '- '-)   ))
         (createRow n br1 (+ br2 1) lista))

            ;polje na koje ne moze da se igra
           (append (list   (list  (floor br2) '()))
         (createRow n br1 (+ br2 1) lista))
                           )   
            )
        ((equalp br1 n)
          (if (zerop (mod br2 2))

(append (list  (list  (floor br2) (list '- '- '- '- '- '- '- '- '-)   ))
         (createRow n br1 (+ br2 1) lista))

         (append (list  (list (floor br2) '()) )
         (createRow n br1 (+ br2 1) lista)) 
         )
            )
            ;potpada pod cond ovo je isto uslov
            ((and (zerop (mod br1 2)) (zerop (mod br2 2)))
                    (append (list   (list (floor br2) (list '- '- '- '- '- '- '- '- 'X) ))
                    (createRow n br1 (+ br2 1) lista))
            )
            (
                (and (not (zerop (mod br1 2))) (not (zerop (mod br2 2))))
                 (append (list (list (floor br2) (list '- '- '- '- '- '- '- '- 'O) ) )
                    (createRow n br1 (+ br2 1) lista))
            )
            (t 
              (append (list  (list (floor br2) '()))
         (createRow n br1 (+ br2 1) lista))
            )
         
         )
    
     )
     (t  (> (+ n 1) br1)
         lista
         )
 )
)

(defun isNumberOfStackGreaterThanHalf ()

(cond 
    ( (equalp numberOfStacksX  (/ (+  (/  (* ( / (floor n) 2) ( - (floor n) 2 )) 8 ) 1 ) 2)) t )
    ( (equalp numberOfStacksO  (/ (+  (/  (* ( / (floor n) 2) ( - (floor n) 2 )) 8 ) 1 ) 2)) t )
    (t '())
)

)



;funkcija za odredjivanje velicine tabele
;n je velicina table, q je pitanje
(defun sizeOfTable (q) ;unos teksta
	(princ q )
	(setq n (read))
	(if (and (numberp n)(> (floor n) 7) ( zerop ( mod( * ( / (floor n) 2) ( - (floor n) 2 )) 8 )) )  (floor n)
		(and (write-line "The number is invalid." )  (sizeOfTable q))
    )

;kako koji igrac odnese pobedu nad stackom(napuni stack i vrh stacka je njegova plocica) tako se ove donje promenljive inkrementiraju
 (setq numberOfStacksX '0)
 (setq numberOfStacksO '0)

    (setq lista (createTable n '1 '1 '()))
    (choosePlayer "Enter 1 for player X or 0 for player O: ")
    (printWholePicture n lista)
    (getPlayFromConsole)

)   

;funkcija u kojoj igrac bira da li hoce da igra
; igrac je promenljiva u kojoj se nalazi X ili O
(defun choosePlayer (q)
   (princ q)
   (setq nextPlaying 'X)
  (setq player1 (read))
 (if (and (numberp player1)(equalp player1 1) )  (and (setq player1 'X) (setq player2 'O) (write-line "You are player X and you play first.")) 
 (if (and (numberp player1) (equalp player1 0)) (and (setq player1 'O) (setq player2 'X) (write-line "You are player O and you play second.") ) (choosePlayer q)))

 )


(defun printWholePicture (n lista)
(princ #\space)
(princ #\space)
(princ #\space)
(princ #\space)
	(loop for i from 1 to n 
		do
        (princ #\space)
        (princ i)
        (princ #\space)
        (princ #\space)

    )
    (princ #\newline)
    (princ #\newline)
    (printTable n lista)

)


(defun printTable (n lista)

        (if (not  (null  lista)) 
            (and 
            (printList (car lista))
            (printTable n (cdr lista))
            )
     )
)

(defun printList (lista)
(setq letter (car lista))
(princ #\space)
(princ #\space)
(princ #\space)
(preparingStackForPrinting 0 (cdr lista) )
(princ #\newline)
(princ (car (list (intern (string (code-char (+ letter 64)))))))
(princ #\space)
(princ #\space)
(preparingStackForPrinting 3 (cdr lista) )
(princ #\newline)
(princ #\space)
(princ #\space)
(princ #\space)
(preparingStackForPrinting 6 (cdr lista) )
(princ #\newline)
)



(defun preparingStackForPrinting (startingPosition lista )
   
    (printStack startingPosition (cadar lista ) )
    (if (not  (null (cdr lista))) 
    (preparingStackForPrinting startingPosition (cdr lista) ) )
)


(defun printStack (startingPosition stack )

(cond
    ((null stack )
        (princ #\space)
        (princ #\space)
        (princ #\space)
        (princ #\space)
    )
    (t
        (princ #\space)
        (princ (nth startingPosition stack))
        (princ (nth (+ 1 startingPosition) stack))
        (princ (nth (+ 2 startingPosition) stack))
    ))
   
)

;ova funkcija sluzi za odredjivanje najvalidnijih poteza, npr. moze se desiti da su ostale funkcije nabavile nekoliko validnih poteza,
;s tim sto su neki potezi dalji od drugih, ova funkcija se stara da te dalje poteze izbrise,
;donja funkcija(ModifyResultPlay) je nastavak ove logike
(defun ModifyCounter (listofplays tmpstack resultListOfPlays)
    (cond 
    ((and (null listofplays)(null tmpstack)) resultListOfPlays)
    
    ((equalp 1 (nth 3 (cadar listofplays)))
        (if 
        (null tmpstack)
            (append resultListOfPlays (list (car listofplays)) (ModifyCounter (cdr listofplays) tmpstack resultListOfPlays))
        
            (append resultListOfPlays (ModifyResultPlay tmpstack '() 99)  (ModifyCounter listofplays '() resultListOfPlays))
        
        )
    )
    ((null tmpstack)
                ;ubacuje se u tmpstack potez i poziva se rekurija
            (ModifyCounter (cdr listofplays) (list  (car listofplays)) resultListOfPlays)
    
    )
    ((listp (caar tmpstack))
        (if (equal (caar tmpstack) (caar listofplays))
            
                (ModifyCounter (cdr listofplays) (append tmpstack (list  (car listofplays))) resultListOfPlays)
            
                (append resultListOfPlays (ModifyResultPlay tmpstack '() 99)
                    (ModifyCounter (cdr listofplays) (car listofplays) resultListOfPlays))
        )
    )
    ( (not (listp (caar tmpstack)))
        (if (equal (car tmpstack) (caar listofplays))
                (ModifyCounter (cdr listofplays) (append (list  tmpstack) (list  (car listofplays))) resultListOfPlays)

                (append resultListOfPlays (ModifyResultPlay (list tmpstack) '() 99)
                    (ModifyCounter (cdr listofplays) (car listofplays) resultListOfPlays))
        )
    )
    (t
    (princ "t je zbog cond-a")
    )
)
)

 (defun ModifyResultPlay (tmpstack result absoluteValue)
    ;prvo se poziva sa ralikom 99, pa onda pitamo da li je razlika =, ako jeste onda i taj potez(jer verovatno ima i prethodni sa istom razlikom)
    ;upisujemo u result jer su oba podjednako validna, ako nije jednako pitamo da li je razlika manja, ako je manja to je validniji potez,
    ;ako nije manja odbacujemo taj potez
    ;u manjoj treba da ocistimo stack i upisemo manju
(cond 
    ((null tmpstack) result)
    
    ((>= (abs (- (caaar tmpstack) (caadar tmpstack))) 3)
        (if (null result)
            (append result tmpstack)

            result        
        )
    )
    ((equalp absoluteValue
        (+  (abs (- (caaar tmpstack) (caadar tmpstack)))
            (abs (- (cadaar tmpstack) (car (cdadar tmpstack))))
        )
    )
        (ModifyResultPlay (cdr tmpstack) (append  result (list  (car tmpstack)) )   absoluteValue)
    )
    (
    (< (+  (abs (- (caaar tmpstack) (caadar tmpstack)))
            (abs (- (cadaar tmpstack) (car (cdadar tmpstack))))
        ) absoluteValue)

        (ModifyResultPlay (cdr tmpstack) (list  (car tmpstack) ) (+  (abs (- (caaar tmpstack) (caadar tmpstack)))
            (abs (- (cadaar tmpstack) (car (cdadar tmpstack))))
                                                                )
        )
    )

    (t 
        (ModifyResultPlay (cdr tmpstack) result absoluteValue)
    )
)
)
 
(defun ModifyListOfLegalPlaysTraverse (listofplays resultListOfPlays)
(cond
    ((null listofplays) resultListOfPlays)
    (t
        (append resultListOfPlays
            (ModifyListOfLegalPlays (car listofplays) resultListOfPlays)     
            (ModifyListOfLegalPlaysTraverse (cdr listofplays) resultListOfPlays))
    )
)
)

;ova funkcija pravi medjupoteze, tako sto dobije potez i prvi njegov najblizi potez(stack), s tim sto taj najblizi potez moze biti
;udaljen sa korakom 2, ova funkcija se stara da napravimo prvi naredni validni medjupotez, sto znaci da skoci na prazno polje
;sa visinom 0
(defun ModifyListOfLegalPlays (firstElementOfListOfPlays resultList)
(cond 
    ((null firstElementOfListOfPlays) resultList)
    (   (and (equal (caar firstElementOfListOfPlays) (caadr firstElementOfListOfPlays) )
            (equal (cadar firstElementOfListOfPlays) (cadadr firstElementOfListOfPlays) )
        )
        (append resultList (list 
        (list (car firstElementOfListOfPlays)
         (list (caadr firstElementOfListOfPlays) (cadadr firstElementOfListOfPlays) (nth 2 (cadr firstElementOfListOfPlays))  ) )   )
    ))
    ((isDiagonal (car firstElementOfListOfPlays) (cadr firstElementOfListOfPlays))         
            (append resultList (list 
                (list (car firstElementOfListOfPlays)
         (list (caadr firstElementOfListOfPlays) (cadadr firstElementOfListOfPlays) (nth 2 (cadr firstElementOfListOfPlays))  ) )
    ))
    )
    ((< (caar firstElementOfListOfPlays) (caadr firstElementOfListOfPlays) )
        (if (equal (cadar firstElementOfListOfPlays) (cadadr firstElementOfListOfPlays) )

            (if (or (equalp (cadar firstElementOfListOfPlays) 1) (equalp (cadar firstElementOfListOfPlays) n) )
                (if  (equalp (cadar firstElementOfListOfPlays) 1)
                
                    (append resultList (list  (list (car firstElementOfListOfPlays)
                        (list (+ (caar firstElementOfListOfPlays) 1) (+ (cadar firstElementOfListOfPlays) 1) 0 )
                    )))
                 (append resultList (list  (list (car firstElementOfListOfPlays)
                    (list (+ (caar firstElementOfListOfPlays) 1) (- (cadar firstElementOfListOfPlays) 1) 0)
                    )))
            )
        (append resultList (list  (list (car firstElementOfListOfPlays)
                (list (+ (caar firstElementOfListOfPlays) 1) (+ (cadar firstElementOfListOfPlays) 1) 0 ) )
                (list (car firstElementOfListOfPlays)
                (list (+ (caar firstElementOfListOfPlays) 1) (- (cadar firstElementOfListOfPlays) 1) 0 ) )
                
            ) )
            )
            
            (if (< (cadar firstElementOfListOfPlays) (cadadr firstElementOfListOfPlays) )
                    (append resultList (list  (list (car firstElementOfListOfPlays)
                        (list (+ (caar firstElementOfListOfPlays) 1) (+ (cadar firstElementOfListOfPlays) 1) 0 )
                    )))

                    (append resultList (list  (list (car firstElementOfListOfPlays)
                    (list (+ (caar firstElementOfListOfPlays) 1) (- (cadar firstElementOfListOfPlays) 1) 0)
                    )))
                )
        )
    )

    ((> (caar firstElementOfListOfPlays) (caadr firstElementOfListOfPlays) )
        
        (if (equal (cadar firstElementOfListOfPlays) (cadadr firstElementOfListOfPlays) )
            (if (or (equalp (cadar firstElementOfListOfPlays) 1) (equalp (cadar firstElementOfListOfPlays) n) )
                (if  (equalp (cadar firstElementOfListOfPlays) 1)

                    (append resultList (list  (list (car firstElementOfListOfPlays)
                        (list (- (caar firstElementOfListOfPlays) 1) (+ (cadar firstElementOfListOfPlays) 1) 0 )
                    )))

                    (append resultList (list  (list (car firstElementOfListOfPlays)
                        (list (- (caar firstElementOfListOfPlays) 1) (- (cadar firstElementOfListOfPlays) 1) 0)
                    ))) 
               
                )


             (append resultList (list  (list (car firstElementOfListOfPlays)
                (list  (- (caar firstElementOfListOfPlays) 1) (+ (cadar firstElementOfListOfPlays) 1) 0) )
            (list (car firstElementOfListOfPlays)
                (list (- (caar firstElementOfListOfPlays) 1) (- (cadar firstElementOfListOfPlays) 1) 0) )
          
              ))
            )
            (if (< (cadar firstElementOfListOfPlays) (cadadr firstElementOfListOfPlays) )
                (append resultList (list  (list (car firstElementOfListOfPlays)
                    (list (- (caar firstElementOfListOfPlays) 1) (+ (cadar firstElementOfListOfPlays) 1) 0)
                )))
                (append resultList (list  (list (car firstElementOfListOfPlays)
                    (list (- (caar firstElementOfListOfPlays) 1) (- (cadar firstElementOfListOfPlays) 1) 0)
                )))
            )
        )
    )
    (t  ;ovde upada kada su jednake vrste posto nema sta drugo 

        (if (or (equalp (caar firstElementOfListOfPlays) 1) (equalp (caar firstElementOfListOfPlays) n) )
            ;tacno
            (if (equalp (caar firstElementOfListOfPlays) 1)
                ;tacno
                (if (< (cadar firstElementOfListOfPlays) (cadadr firstElementOfListOfPlays) )    
                    (append resultList (list  (list (car firstElementOfListOfPlays)
                        (list (+ (caar firstElementOfListOfPlays) 1) (+ (cadar firstElementOfListOfPlays) 1) 0)
                    )))

                    (append resultList (list  (list (car firstElementOfListOfPlays)
                        (list (+ (caar firstElementOfListOfPlays) 1) (- (cadar firstElementOfListOfPlays) 1) 0)
                    )))
                )
                ;netacno
                (if (< (cadar firstElementOfListOfPlays) (cadadr firstElementOfListOfPlays) )    
                    (append resultList (list  (list (car firstElementOfListOfPlays)
                        (list (- (caar firstElementOfListOfPlays) 1) (+ (cadar firstElementOfListOfPlays) 1) 0)
                    )))

                    (append resultList (list  (list (car firstElementOfListOfPlays)
                        (list (- (caar firstElementOfListOfPlays) 1) (- (cadar firstElementOfListOfPlays) 1) 0)
                    )))
                )    
            
            )
            ;netacno   
            (if (< (cadar firstElementOfListOfPlays) (cadadr firstElementOfListOfPlays) )    
                    (append resultList (list  (list (car firstElementOfListOfPlays)
                        (list (+ (caar firstElementOfListOfPlays) 1) (+ (cadar firstElementOfListOfPlays) 1) 0 ) )
                        (list (car firstElementOfListOfPlays)
                        (list (- (caar firstElementOfListOfPlays) 1) (+ (cadar firstElementOfListOfPlays) 1) 0 ) )
                    
                    ) )
                    (append resultList (list  (list (car firstElementOfListOfPlays)
                        (list (+ (caar firstElementOfListOfPlays) 1) (- (cadar firstElementOfListOfPlays) 1) 0 ) )
                        (list (car firstElementOfListOfPlays)
                        (list (- (caar firstElementOfListOfPlays) 1) (- (cadar firstElementOfListOfPlays) 1) 0 ) )
                    ) )
            )
        )
    )
)
)

(defun isDiagonal(currentlist nextlist)
  (cond ((null nextlist) '())

        ( (and (equalp  (- (car currentlist) 1) (car nextlist)) (equalp  (- (cadr currentlist) 1) (cadr nextlist))
        )
         t)
         
         ( (and (equalp  (- (car currentlist) 1) (car nextlist)) (equalp  (+ (cadr currentlist) 1) (cadr nextlist))
        )
         t)

         ( (and (equalp  (+ (car currentlist) 1) (car nextlist)) (equalp  (- (cadr currentlist) 1) (cadr nextlist))
        )
         t)
         ( (and (equalp  (+ (car currentlist) 1) (car nextlist)) (equalp  (+ (cadr currentlist) 1) (cadr nextlist))
        )
         t)

    (t '())
  ))




;kasnije dodato
(defun ModifyPlay (firstElementOfListOfPlays resultList)
(cond 
    ((null firstElementOfListOfPlays) resultList)
    (   (and (equal (caar firstElementOfListOfPlays) (caadr firstElementOfListOfPlays) )
            (equal (cadar firstElementOfListOfPlays) (cadadr firstElementOfListOfPlays) )
        )
        (append resultList
        (list (car firstElementOfListOfPlays)
         (list (caadr firstElementOfListOfPlays) (cadadr firstElementOfListOfPlays) (nth 2 (cadr firstElementOfListOfPlays))  ) )   )
    )
    ((isDiagonal (car firstElementOfListOfPlays) (cadr firstElementOfListOfPlays))         
            (append resultList
                (list (car firstElementOfListOfPlays)
         (list (caadr firstElementOfListOfPlays) (cadadr firstElementOfListOfPlays) (nth 2 (cadr firstElementOfListOfPlays))  ) )
    )
    )

    (t 
    (if (< (caar firstElementOfListOfPlays) (caadr firstElementOfListOfPlays) )
        
        (if (equal (cadar firstElementOfListOfPlays) (cadadr firstElementOfListOfPlays) )
            (append resultList  (list  
                (list (car firstElementOfListOfPlays)
                (list (+ (caar firstElementOfListOfPlays) 1) (+ (cadar firstElementOfListOfPlays) 1) 0 ) )
                (list (car firstElementOfListOfPlays)
                (list (+ (caar firstElementOfListOfPlays) 1) (- (cadar firstElementOfListOfPlays) 1) 0 ) )
                )
            ) 
                (if (< (cadar firstElementOfListOfPlays) (cadadr firstElementOfListOfPlays) )
                    (list (+ (caar firstElementOfListOfPlays) 1) (+ (cadar firstElementOfListOfPlays) 1) )

                    (list (+ (caar firstElementOfListOfPlays) 1) (- (cadar firstElementOfListOfPlays) 1) )
                )
        )
    )

    (if (> (caar firstElementOfListOfPlays) (caadr firstElementOfListOfPlays) )
        
        (if (equal (cadar firstElementOfListOfPlays) (cadadr firstElementOfListOfPlays) )
            (list  (list (- (caar firstElementOfListOfPlays) 1) (+ (cadar firstElementOfListOfPlays) 1) )
             (list (- (caar firstElementOfListOfPlays) 1) (- (cadar firstElementOfListOfPlays) 1) ) )

            (if (< (cadar firstElementOfListOfPlays) (cadadr firstElementOfListOfPlays) )
                (list (- (caar firstElementOfListOfPlays) 1) (+ (cadar firstElementOfListOfPlays) 1) )

                (list (- (caar firstElementOfListOfPlays) 1) (- (cadar firstElementOfListOfPlays) 1) )
            )
        )
    )
)
)
)

;ova funkcija sluzi da uredi listu koju vrati funkcija za pronalazenje poteza
(defun ModifyListOfPossiblePlays (positionForMaking listofplays modifiedList)
    (cond
        ((null  listofplays) modifiedList)
        ((and (null  positionForMaking) (not (listp (nth 3 listofplays))) )
            (append modifiedList
                (list 
                    (list  
                        (list (car listofplays) (cadr listofplays) (nth 2 listofplays) ) 
                            (list 
                                (nth 3 listofplays) (nth 4 listofplays) (nth 5 listofplays)
                            )
                    )
                    (list  
                        (list 
                            (nth 3 listofplays) (nth 4 listofplays) (nth 5 listofplays)
                        )
                            (list (car listofplays) (cadr listofplays) (nth 2 listofplays) )
                    )
                )
            )
        )
        
        ((null  positionForMaking) 
            (append modifiedList
            (list 

            (list  
            (list (car listofplays) (cadr listofplays) (nth 2 listofplays) ) 
            (nth 3 listofplays)
           )
            )

           (ModifyListOfPossiblePlays (list (car listofplays) (cadr listofplays) (nth 2 listofplays) ) (cddddr listofplays) modifiedList)
            )
        )
        ((listp (car listofplays))
            (append modifiedList
            
            (list 
            (list  
                positionForMaking 
            (car listofplays)
           )

            )

           (ModifyListOfPossiblePlays positionForMaking (cdr listofplays) modifiedList)
            )
        )
        (t
            (append modifiedList
            
            (list 
            
            (list  
            (list (car listofplays) (cadr listofplays) (nth 2 listofplays) ) 
            (nth 3 listofplays)
           )

            )

           (ModifyListOfPossiblePlays (list (car listofplays) (cadr listofplays) (nth 2 listofplays) ) (cddddr listofplays) modifiedList)
            )
        )
    )
)


(defun MakeListOfAllPossiblePlaysForPlayer (lista listofplays n nextPlaying resultList)
(cond ((null nextPlaying) '() )
    ((null lista) resultList)
    ((null listofplays) resultList)
    (t 
        (let ( ( stackForProcessing (removeLines (findStack lista (list (caaar listofplays) (cadaar listofplays))) ) ) )
        
           (append resultList (RefactorPairs (MakePossiblePlaysForGivenStacks (list (caaar listofplays) (cadaar listofplays)) stackForProcessing (length stackForProcessing) (cadar listofplays) nextPlaying resultList) '())
           
           (MakeListOfAllPossiblePlaysForPlayer lista (cdr listofplays) n nextPlaying resultList)
           
           )
           )
           
        )
            
         
        )
)    


;X   5
;X   4
;X   3   3
;X   2   2
;X   1   1
;O   0   0
(defun MakePossiblePlaysForGivenStacks (sourceplay stackForProcessing initialLength destinationplay nextPlaying resultList)
(cond ((null nextPlaying) '() )
    ((null stackForProcessing) resultList)

    ((equalp 0 (nth 2 destinationplay))
        (if (equal nextPlaying (nth (- initialLength 1) stackForProcessing)) 
            (append resultList  (list  
                (list (car sourceplay) (cadr sourceplay) '0 ) destinationplay) 
            )
        )
    )
    (t
        (if (member nextPlaying stackForProcessing)
           (if 
                (and 
                (>= 8  (+ (caddr destinationplay) (- initialLength (-  (length  (member nextPlaying stackForProcessing)) 1)) )  )
                (<  (-  (length  (member nextPlaying stackForProcessing)) 1) 
                (caddr destinationplay)) ;treba da ostane npr. lenght koji je 6 jer ce taj npr. X da skoci zapravo na visinu 6
                )

           (append resultList  (list  
            (list (car sourceplay) (cadr sourceplay) (-  (length  (member nextPlaying stackForProcessing)) 1 ) ) destinationplay
           )
           (MakePossiblePlaysForGivenStacks sourceplay (cdr (member nextPlaying stackForProcessing)) initialLength destinationplay nextPlaying resultList)
           )
            (MakePossiblePlaysForGivenStacks sourceplay (cdr (member nextPlaying stackForProcessing)) initialLength destinationplay nextPlaying resultList)
            ) 
            (MakePossiblePlaysForGivenStacks sourceplay '() initialLength destinationplay nextPlaying resultList)
        )
    )
)
)

(defun RefactorPairs (listforrefactoring resultList)
(cond  
    ((null listforrefactoring) resultList)
    (t
        (append resultList   (list (list (car listforrefactoring) (cadr listforrefactoring)))
            (RefactorPairs (cddr listforrefactoring) resultList)  
        ) 
    )
)
)

;funkcija za pocetak igre
(defun start()
(sizeOfTable '"Enter size of the table: ")
)
